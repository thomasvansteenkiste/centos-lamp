#!/bin/bash

echo "Is de virtuele machine aan het runnen via Vagrant?"

vagrant status

echo "Zijn we op de centos64 box aan het werken?"

vagrant box list

echo "Is de VM ge�nstalleerd op de gevraagde plaats (Linux/centos-lamp)"

if [ -d "/" ]; then
	echo " VM op juiste plaats ge�nstalleerd."
else echo "VM directory niet aanwezig."

fi

echo "Werkt puppet ?"

result="$(vagrant provision --provision-with puppet)"
if [ -n "{result}" ]; then
	echo " Puppet werkt correct!"
fi

echo "Toon de commits via GIT"

git log

